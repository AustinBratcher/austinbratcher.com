var main = $('.main_content');

function Blog(body) {
	this.body = body;
	this.allPosts = null;
	this.abstractTemplate = this.body.find('.postAbstract.template');
}
	
Blog.prototype.addSeperation = function (){
	this.body.append('<hr>');
}

Blog.prototype.renderAbstract = function (post) {
	var th = this;
	var newAbstract = th.abstractTemplate.clone().removeClass('template');
	newAbstract.find('.title').text(post.name);
	newAbstract.find('.category').text(post.category);
	newAbstract.find('.abstract').text(post.abstract);
	newAbstract.find('.taglist').text(post.keywords.join(', '));
	newAbstract.data('details', post);
	th.getAbstractImage(function(){
		newAbstract.find('#thumbnail').attr('src', '/blog/post/thumbnail/'+post.postID);
	});
	return newAbstract;
}

Blog.prototype.showPost = function(postID) {
	var th = this;

	th.getPostDetails(postID, function(post){
		if(!post) return;
		var route = '/blog/post/' + post.postID;

		th.getFullPost(post.postID, function(postHtml){
			th.body.html(postHtml);
			window.history.pushState({main_html: main.html()}, post.postID, route);
		});
	});
}

// Will want to create some form of filtering
Blog.prototype.listPosts = function() {
	var th = this;

	var renderPosts = function() {
		var abstracts = [];
		for(var i in th.allPosts){
			abstracts[i] = th.renderAbstract(th.allPosts[i]);
			var abstract = abstracts[i];
			th.body.append(abstracts[i]);
		}		
		th.body.find('.title,.imgContainer').click(function(){
			//bind click to specific title/image
			// grab data from parent element	
			th.showPost($(this).parents('.postAbstract').data('details').postID);
		});

		//really dumb last child hack because of foundation
		abstracts[abstracts.length] = th.abstractTemplate.clone();
		th.body.append(abstracts[abstracts.length-1]);

		for(var i in abstracts){
			function runIt(){
				abstracts[i].animate({"left": "-=1500px"}, "slow").removeClass('new');
			}
			function showIt(){
				abstracts[i].delay(50*i)
			}
			runIt();
			showIt();
		}
	}

	if(!th.allPosts) {
		th.getAllPosts(renderPosts);
	}
	else {
		renderPosts(th.allPosts);
	}
}

// Blog.prototype.getAbstractImage = function(postID, callback) {
// 	var th = this;
// 	$.ajax({ 
// 		type: 'GET',
// 		//dataType: 'json',
// 		url: '/blog/post/thumbnail/' + postID,
// 		success: function(data){
// 			console.log(data);
// 			if(callback) callback(data);
// 		}
// 	});
// }
Blog.prototype.getAbstractImage = function(callback) {
	if(callback) callback();
}

Blog.prototype.getPostDetails = function(postID, callback) {
	var th = this;
	$.ajax({ 
		type: 'GET',
		dataType: 'json',
		url: '/blog/post/details/' + postID,
		success: function(data){
			if(callback) callback(data);
		}
	});
}

Blog.prototype.getFullPost = function(postID, callback) {
	var th = this;
	$.ajax({ 
		type: 'GET',
		dataType: 'html',
		url: '/blog/post/content/'+postID,
		success: function(data){
			if(callback) callback(data);
		}
	});
}


Blog.prototype.getAllPosts = function(callback) {
	var th = this;
	if(!th.allPosts){
		$.ajax({ 
			type: 'GET',
			dataType: 'json',
			url: '/blog/allposts', // issue when page is loaded from navbar
			success: function(data){
				if(!th.allPosts) th.allPosts = data;
				if(callback) callback(th.allPosts);
			}
		});
	} else callback(th.allPosts);
}
