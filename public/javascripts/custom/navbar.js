var main = $('.main_content');

window.addEventListener('popstate', function (event) {
  main.html(event.state.main_html);
});


function changeURL(ajaxUrl, title, route) {
	$.ajax({ 
		type: 'GET',
		dataType: 'html',
		url: ajaxUrl,
		success: function(data){
			main.html(data);
			window.history.pushState({main_html: main.html()}, title, route);
		}
	});
}


$( '.home_nav' ).click(function(e) {	
	changeURL('/body/home', 'Austin Bratcher', '/');
	e.preventDefault();
});

$( '.blog_nav' ).click(function(e) {
	changeURL('/body/blog', 'Blog', '/blog');
	e.preventDefault();
	//$.getScript('/blog/blogjs');
});

$( '.interests_nav' ).click(function(e) {
	changeURL('/body/interests', 'Interests', '/interests')
	e.preventDefault();
});

$( '.bible_nav' ).click(function(e) {
	changeURL('/body/bible', 'Bible + Spritz', '/bible');
	e.preventDefault()
});
var drop_clicked=false;
$( '.resumes_nav' ).click(function(e) {
	if(!drop_clicked){
		changeURL('body/resumes', 'Resumes', '/resumes');
		e.preventDefault();
	}
	drop_clicked = false;
});

$( '.min_resume_nav' ).click(function(e) {
	drop_clicked = true;
	changeURL('/body/ministry_resume', 'Ministry Resume', '/resumes/ministry');
	e.preventDefault();
});

$( '.prof_resume_nav' ).click(function(e) {
	drop_clicked = true;
	changeURL('/body/software_resume', 'Software Resume', '/resumes/software');
	e.preventDefault();
});

$( '.contact_nav' ).click(function(e) {
	changeURL('/body/contact', 'Contact Me', '/contact');
	e.preventDefault();
});

//give initial content to use
history.replaceState({main_html: main.html()}, 'Austin Bratcher', document.location.href);