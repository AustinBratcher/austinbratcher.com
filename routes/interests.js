var express = require('express');
var router = express.Router();

/* GET sermons home page. */
router.get('/', function(req, res) {
	res.render('interests', { title: 'Interests' } );
});

module.exports = router;
