var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/home', function(req, res) {
   //var db = req.db;
   //var imgcollection = db.get('images')
   // db.collection('images').findOne({"name": "Banner"}, function(err, result) {
  	// 	res.render('body_content/home_body', { title: 'Austin Bratcher', banner: result.path} );
   // });

   var db = req.db;
   var callbacksComplted = 0;
   var callbacksExpected = 3;
   var images = {};

   function renderPage() {
      if(callbacksComplted >= callbacksExpected) {
         res.render('body_content/home_body', { title: 'Austin Bratcher', banner: images['banner'], 
             computer: images['computer'], truett: images['truett'] });
      }
   }
   function dbCallComplete() {
      callbacksComplted++;
      renderPage();
   }

   db.collection('images').findOne({"name": "Banner"}, function(err, result) {   
      images['banner'] = result.path;
      dbCallComplete();
   });
   db.collection('images').findOne({"name": "spain-supercomputer"}, function(err, result) {
      images['computer'] = result.path;
      dbCallComplete();
   });
   db.collection('images').findOne({"name": "truett"}, function(err, result) {
      images['truett'] = result.path;
      dbCallComplete();
   });
});

router.get('/blog', function(req, res) {
   var db = req.db;
	res.render('body_content/blog_body', { title: 'Blog'} );
});

router.get('/interests', function(req, res) {
   var db = req.db;
	res.render('body_content/interests_body', { title: 'Interests'} );
});

router.get('/bible', function(req, res) {
   var db = req.db;
	res.render('body_content/bible_body', { title: 'Bible + Spritz'} );
});

router.get('/resumes', function(req, res) {
   var db = req.db;
	res.render('body_content/resumes_body', { title: 'Resumes'} );
});

router.get('/ministry_resume', function(req, res) {
   var db = req.db;
	res.render('body_content/min_resume_body', { title: 'Ministry Resume'} );
});

router.get('/software_resume', function(req, res) {
   var db = req.db;
	res.render('body_content/prof_resume_body', { title: 'Software Resume'} );
});

router.get('/contact', function(req, res) {
   var db = req.db;
	res.render('body_content/contact_me_body', { title: 'Contact Me'} );
});


module.exports = router;
