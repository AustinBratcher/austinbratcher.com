var express = require('express');
var router = express.Router();

/* GET sermons home page. */
router.get('/', function(req, res) {
	res.render('bible', { title: 'Bible + Spritz' } );
});

module.exports = router;
