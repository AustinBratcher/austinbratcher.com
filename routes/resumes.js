var express = require('express');
var router = express.Router();

/* GET sermons home page. */
router.get('/', function(req, res) {
	res.render('resumes', { title: 'Resumes' } );
});

router.get('/ministry', function(req, res) {
	res.render('min_resume', { title: 'Ministry Resume' } );
});

router.get('/software', function(req, res) {
	res.render('prof_resume', { title: 'Softare Resume' } );
});

module.exports = router;
