var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	var db = req.db;
	var callbacksComplted = 0;
	var callbacksExpected = 3;
	var images = {};

	function renderPage() {
		if(callbacksComplted >= callbacksExpected) {
			res.render('index', { title: 'Austin Bratcher', banner: images['banner'], 
				 computer: images['computer'], truett: images['truett'] });
		}
	}
	function dbCallComplete() {
		callbacksComplted++;
		renderPage();
	}

	db.collection('images').findOne({"name": "Banner"}, function(err, result) {	
		images['banner'] = result.path;
		dbCallComplete();
	});
	db.collection('images').findOne({"name": "spain-supercomputer"}, function(err, result) {
		images['computer'] = result.path;
		dbCallComplete();
	});
	db.collection('images').findOne({"name": "truett"}, function(err, result) {
		images['truett'] = result.path;
		dbCallComplete();
	});
});

router.get('/contact', function(req, res) {
	res.render('contact_me', { title: 'Contact Me'} );
});

router.get('/update_eating', function(req, res) {
	res.render('update_eating', { title: 'Update Eating location'} );
});
router.get('/whereareweeating', function(req, res) {
        var db = req.db;
	db.collection('eating').findOne({"id": "eating"}, function(err, result) {
		res.render('eating', { title: 'Where are we eating this week?', loc: result.loc } );
	});

});


router.post('/eating_update', function(req, res) {
	var db = req.db;
	var loc  = req.body.loc;
	var postdate = new Date();

	db.collection('eating').update( {"id" : 'eating'},
	{
		"id" : 'eating',
		"loc" : loc, 
		"postdate" : postdate
	},
	{ upsert:true},
	function(err, result){
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            res.location("Update Eating Location -- Success");
            // And forward to success page
            res.redirect("whereareweeating");
        }
	});
});

router.get('/image/add', function(req, res) {
	res.render('add_image', { title: 'Add Image'} );
});

router.post('/image/add', function(req, res) {
	var db = req.db;
	var name = req.body.imagename;
	var path = req.body.imagepath;
	var desc = req.body.desc;
	var postdate = new Date();

	db.collection('images').insert( {
		"name" : name, 
		"path" : path,
		"description" : desc,
		"postdate" : postdate
	}, function(err, result){
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            res.location("Add Post - Success");
            // And forward to success page
            res.redirect("add");
        }
	});
});

module.exports = router;
