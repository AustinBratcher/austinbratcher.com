var express = require('express');
var jade = require('jade');
var constants = require('../public/javascripts/constants.js')
var router = express.Router();
ObjectID = require('mongoskin').ObjectID;

/* GET sermons home page. */
router.get('/', function(req, res) {
	res.render('blog', { title: 'Blog' } );
});

router.get('/allposts', function(req, res) {
	var db = req.db;

//	db.collection('blogPosts').find({}, {'name': 0, 'keywords':0, 'path':0}).toArray(function(err, result) {	
	db.collection('blogPosts').find({}).toArray(function(err, result) {	
		res.json(result);
	});
});

// Gets returns object with all info for the post
router.get('/post/details/:postID', function(req, res) {
	var db = req.db;
	db.collection('blogPosts').findOne({"postID": req.param('postID').toLowerCase()}, function(err, result) {
		if(err){
			console.log("error on lookup")
            res.send("There was a problem adding the information to the database.");
		}
		else {
			console.log(result);
			res.json(result);
		}
	});
});

// gets the jade content of a particular post. 
router.get('/post/content/:postID', function(req, res) {
	//res.render('blog_posts/blog_template');
	var db = req.db;
	// db.collection('blogPosts').findOne({"_id": new ObjectID(req.param('postid'))}, function(err, result) {
	db.collection('blogPosts').findOne({"postID": req.param('postID').toLowerCase()}, function(err, result) {
		if(err){
			console.log("error on lookup")
            res.send("There was a problem adding the information to the database.");
		}
		else {
			delete result.abstract;
			res.render(result.jadepath);	
		}
	});
});

// Renders page with specific post loaded
router.get('/post/:postID', function(req, res) {
	var db = req.db;
	// db.collection('blogPosts').findOne({"_id": new ObjectID(req.param('postid'))}, function(err, result) {
	db.collection('blogPosts').findOne({"postID": req.param('postID').toLowerCase()}, function(err, result) {
		if(err){
			console.log("error on lookup")
            res.send("There was a problem adding the information to the database.");
		}
		else {
			res.render('load_blog', {toLoad: result.postID});	
		}
	});
});

router.get('/post/thumbnail/:postID', function(req, res) {
	console.log('abstract image hit');
	var db = req.db;
// res.sendfile('/javascripts/custom/blog_body.js', { 'root': __dirname + '/../public'});
	var imagePath = __dirname + '/../public/images'
	db.collection('blogPosts').findOne({"postID": req.param('postID').toLowerCase()}, function(err, result) {
		if(err){
			console.log("error on lookup")
            res.send("There was a problem adding the information to the database.");
		}
		else {
			// if(result.thumbnail) res.attachment(imagePath+result.thumbnail);
			if(result.thumbnail) res.download(result.thumbnail, 
				result.thumbnail.replace(/^.*[\\\/]/, ''), {'root':imagePath});
			else res.download('/blog/placehold.gif', 'placehold.gif', {'root': imagePath});
			//res.render('load_blog', {toLoad: result.postID});	
		}
	});
});


router.get('/blogjs', function(req, res) {
	res.sendfile('/javascripts/custom/blog_body.js', { 'root': __dirname + '/../public'});
});

router.get('/add', function(req, res) {
	res.render('add_blog', {title: 'Add Blog', categories: constants.blog.categories});
});

router.post('/add', function(req, res) {
	var db = req.db;
	var keywords = req.body.keywords.split(',');
	for(var i in keywords) {
		keywords[i] = keywords[i].trim();
	}

	db.collection('blogPosts').insert( {
		"name"       : req.body.postname,
		"postID"     : req.body.postname.replace(/\s+/g,'_').toLowerCase(),
		"category"   : req.body.category,
		"jadepath"   : req.body.jadepath,
		"thumbnail"  : req.body.thumbnail,
		"abstract"   : req.body.abstract,
		"keywords"   : keywords
	}, function(err, result){
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            res.location("Add Post - Success");
            // And forward to success page
            res.redirect("add");
        }
	});
});

router.get('/update', function(req, res) {
	var db = req.db;
	db.collection('blogPosts').find({}, {'abstract':0}).toArray(function(err, result) {
		var titles = [];
		for(var i in result) {
			titles[i] = result[i].name;
		}
		res.render('update_select', {title: 'Update Blog', titles: titles});

	});	

});

router.post('/updateselected', function(req, res) {
	var db = req.db;

	var db = req.db;
	// db.collection('blogPosts').findOne({"_id": new ObjectID(req.param('postid'))}, function(err, result) {
	db.collection('blogPosts').findOne({"name": req.body.name}, function(err, result) {
		if(err){
			console.log("error on lookup")
            res.send("There was a problem adding the information to the database.");
		}
		else {
			
			res.render('update_blog', {
					title      : 'Update Blog', 
					name       : result.name,
					categories : constants.blog.categories,
					category   : result.category,
					keywords   : result.keywords.join(','),
					thumbnail  : result.thumbnail,
					abstract   : result.abstract,
					jadepath   : result.jadepath
				}
			);
		}
	});

});


router.post('/update', function(req, res) {
	var db = req.db;
	var keywords = req.body.keywords.split(',');
	for(var i in keywords) {
		keywords[i] = keywords[i].trim();
	}

	db.collection('blogPosts').update( 
	{   "postID" : req.body.originalname.replace(/\s+/g,'')},
	{
		"name"       : req.body.postname,
		"postID"     : req.body.postname.replace(/\s+/g,'_').toLowerCase(),
		"category"   : req.body.category,
		"jadepath"   : req.body.jadepath,
		"thumbnail"  : req.body.thumbnail,
		"abstract"   : req.body.abstract,
		"keywords"   : keywords
	}, function(err, result){
        if (err) {
            // If it failed, return error
            res.send("There was a problem adding the information to the database.");
        }
        else {
            // If it worked, set the header so the address bar doesn't still say /adduser
            res.location("Update succeed");
            // And forward to success page
            res.redirect("update");
        }
	});
});

router.get('/delete', function(req, res) {
	var db = req.db;

	db.collection('blogPosts').find({}, {'abstract':0}).toArray(function(err, result) {
		var titles = [];
		for(var i in result) {
			titles[i] = result[i].name;
		}
		res.render('delete_blog', {title: 'Delete Blog', titles: titles});
	});

});

router.post('/delete', function(req, res) {
	var db = req.db;

	//get all blog info
	//insert into new place,
	//delete from old
	db.collection('blogPosts').findOne({"name": req.body.name}, function(err, result) {
		db.collection('blogArchive').insert({
			"name"     : result.name,
			"category" : result.category,
			"jadepath" : result.jadepath,
			"abstract" : result.abstract,
			"keywords" : result.keywords
		}, function(err, res2){
			db.collection('blogPosts').remove({"name" : req.body.name}, function(err, result) {
		        if (err) {
		            // If it failed, return error
		            res.send("There was a problem removing the information to the database.");
		        }
		        else {
		            // If it worked, set the header so the address bar doesn't still say /adduser
		            res.location("Delete succeed");
		            // And forward to success page
		            res.redirect("delete");
		        }
			});
		});
	});
});

module.exports = router;
